﻿#include <iostream>

const int n = 30; // глобальная константа на количество чисел в цикле

void printNumbers(bool choice)				//choice - локальная переменная, поэтому обьявляем второй раз
{
	if (choice)
		std::cout << "Even numbers: \n";
	else
		std::cout << "Odd numbers: \n";

	int zero = 0;							//добавляем переменную для сравнения с остатком от деления
	for (int i = 0; i <= n; i++)
		if (choice)
		{
			if (zero == i % 2)				//если остаток равен нулю, то число чётное и выводим его на экран	
				std::cout << i << "\n";
		}
		else
		{
			if (zero != i % 2)				//если отличен от нуля, то число нечётное и выводим его на экран
				std::cout << i << "\n";
		}
}

int main()
{
	bool choice = true;						//переменная для управления выбором; 1- четные числа, 0 - нечетные числа
								
		
	printNumbers(choice);


	return 0;
}